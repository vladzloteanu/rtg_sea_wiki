# Bugs / Improvements (importante)

## All
- adauga ID-ul campaniei in nume (done pt campanii), eventual cu un prefix [RT.biz]; la fel la audiente, la liste, etc
(pt conturile care au si alte campanii)


## Tracking JS

- TODO: verificat cu contactul adwords, ce se intampla cand clientul
are deja propriul tracking (cazul superpantofi) 


## Product feed
- sa incluzi neaparat sale price (si eventual formatted sale price)


## Audiences

### Lookalike
- Ex: "userlist_Buyer Lookalike Targeting" - e pus pe Visitors of a page with a specific tag
- Adwords iti face el singur lookalike, din pacate :)
Poti doar sa astepti si sa vezi daca ti-a creat-o.
TODO: de confirmat cu contactul Adwords

https://support.google.com/adwords/answer/2676774?hl=en
    
    "Qualifying for similar audiences

    Google will automatically identify which of your remarketing lists qualify for similar audiences based on a variety of factors, including but not limited to the following:

    Minimum of 5,000 visitors on the original list
    How recently these people joined the original list
    The types of sites that these visitors browsed

    AdWords can define a similar audience for your own lists only."

### Re-engage buyers
- nu e definita, nu? (e pusa pe un tag test)
- la ce perioada ne-am hotarat? (ca acum e pus la 180 zile)

### Visitors Remarketing
- nu e definita, nu? (e pusa pe un tag test)

## Expression based user list (cart saver)
- de ce e creata in mai multe exemplare?

## Campaign settings
- pus target pe tara (Romania) - acum e fara target
- bid strategy: target CPA sau target ROAS - acum e manual cpc
- fara start date / end date


## Campaign ads
- cred ca ai o pb de encoding pe target urls: "http://www.gtrafic.ro/index.php?route=product/product&amp;product_id=30"
(probabil din feed) cum sa reproduci: da click pe un ad


# Sugestii / idei

## UI creare / editare campanie

- Headline / Description / Display URL - pus un cate exemplu, in fundal

- Action buttons raman doar in engleza?

- ROI: precompletat la o valoare, explicat concret: 
Ex, daca pune 25%, ii spui: pt 10 lei cheltuiti in adwords, ne asteptam sa
generam vanzari de 40 de lei

- ROI: vezi ca daca nu e sub 100, nu e ROI, e un fel de cost of sales
(ROI e > 100%, ca e income / investment )

- CPA: la fel, ar fi bine sa fie pus la o valoare standard (ROI x 
shopping cart-ul lui mediu)


### Sync Up Errors

Ce se intampla daca unul din elemente nu se sincronizeaza?
Poate fi destul de frecvent, de ex, sa foloseasca un cuvant
cu copyright in ad, si Adwords sa refuze;

Cum arati treaba asta? Se mai pot edita dupa?


## Afisare stats

- Nu ai graf, e doar tabel?
- uneori le numesti tranzactions, alteori conversions;  